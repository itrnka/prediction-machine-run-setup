#!/usr/bin/env bash

# config
PIO_DIR=predictionIO-setup-script

# prepare git
sudo apt-get update
sudo apt-get install git

# download and run
cd ~
if [ -d ${PIO_DIR} ]; then
   sudo rm -rf ${PIO_DIR};
fi
git clone https://itrnka@bitbucket.org/itrnka/prediction-machine-install.git ${PIO_DIR}
#sudo chmod -R +x ${PIO_DIR}/*.sh
find ${PIO_DIR} -type f -iname "*.sh" -exec sudo chmod +x {} \; # make all sh executable

cd ${PIO_DIR}
./install.sh